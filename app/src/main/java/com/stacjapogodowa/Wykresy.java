package com.stacjapogodowa;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wykresy extends Fragment implements FragmentCallbacks
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private FrameLayout frameLayoutWykresy;
    private FrameLayout frameLayoutNaWykres;
    private Spinner spinnerRodzajWykresu;
    private Spinner spinnerRodzajDanych;
    private EditText editTextIloscProbek;
    private List<String> lista;
    private int iloscProbekNaWykresie=0;

    @Override
    public void onMsgFromMainToFragment(String strValue)
    {
        // Tutaj odtworzenie listy - zawartosci bazy danych
        lista = new ArrayList<String>(Arrays.asList(strValue.split(";")));
        rysujWykres();
    }

    private void rysujWykres()
    {
        String rodzajDanych = spinnerRodzajDanych.getSelectedItem().toString();
        // Najpierw zapis wartosci - pewnie do listy floatow
        ArrayList<Double> listaWartosci = new ArrayList<Double>();
        int N = iloscProbekNaWykresie;
        if(lista.size()<N)
        {
            N=lista.size();
        }

        if(rodzajDanych.equals("Temperatura"))
        {
            for(int i = 0; i < N; i++)
            {
                listaWartosci.add(new Double (podajWartosc("Temperatura",lista.get(lista.size()-N+i))));
            }
        }
        else if(rodzajDanych.equals("Ciśnienie"))
        {
            for(int i = 0; i < N; i++)
            {
                listaWartosci.add(new Double (podajWartosc("Cisnienie",lista.get(lista.size()-N+i))));
            }
        }
        else if(rodzajDanych.equals("Wilgotność"))
        {
            for(int i = 0; i < N; i++)
            {
                listaWartosci.add(new Double (podajWartosc("Wilgotnosc",lista.get(lista.size()-N+i))));
            }
        }
        else if(rodzajDanych.equals("Prędkość wiatru"))
        {
            for(int i = 0; i < N; i++)
            {
                listaWartosci.add(podajWartoscDouble("Predkosc wiatru",lista.get(lista.size()-N+i)));
            }
        }
        else if(rodzajDanych.equals("Kierunek wiatru"))
        {
            for(int i = 0; i < N; i++)
            {
                listaWartosci.add(podajWartoscDouble("Kierunek wiatru", lista.get(lista.size() - N + i)));
            }
        }

        frameLayoutNaWykres.removeAllViews();
        if(spinnerRodzajWykresu.getSelectedItem().toString().equals("Liniowy"))
        {
            LineChart chart = new LineChart(getContext());
            frameLayoutNaWykres.addView(chart);
            ArrayList<Entry> entries = new ArrayList<Entry>();

            for(int i = 0; i < listaWartosci.size(); i++)
            {
                float x = i;
                double y = listaWartosci.get(i);
                entries.add(new Entry(x, (float)y));
            }

            // Tutaj uzupelnienie listy - add
            LineDataSet dataset = new LineDataSet(entries, "Wartość");
            LineData lineData = new LineData(dataset);
            chart.setData(lineData);
            Description description = new Description();
            description.setText("");
            chart.setDescription(description);
            chart.invalidate();
        }
        else if(spinnerRodzajWykresu.getSelectedItem().toString().equals("Słupkowy"))
        {
            BarChart chart = new BarChart(getContext());
            frameLayoutNaWykres.addView(chart);
            ArrayList<BarEntry> entries = new ArrayList<BarEntry>();

            for(int i = 0; i < listaWartosci.size(); i++)
            {
                float x = i;
                double y = listaWartosci.get(i);
                entries.add(new BarEntry(x, (float)y));
            }

            // Tutaj uzupelnienie listy - add
            BarDataSet dataset = new BarDataSet(entries, "Wartość");
            ArrayList<IBarDataSet> labels = new ArrayList<IBarDataSet>();
            labels.add(dataset);
            BarData data = new BarData(dataset);
            chart.setData(data);
            Description description = new Description();
            description.setText("");
            chart.setDescription(description);
            chart.invalidate();
        }
    }

    public int podajWartosc(String tekst, String ostatniOdczyt) // Temperatura, Cisnienie, Wilgotnosc
    {
        String jednostka="";
        if(tekst.equals("Cisnienie"))
        {
            jednostka = "hPA";
        }
        else if(tekst.equals("Temperatura"))
        {
            jednostka = "*C";
        }
        else if(tekst.equals("Wilgotnosc"))
        {
            jednostka = "%";
        }
        else if(tekst.equals("Predkosc wiatru"))
        {
            jednostka = "m/s";
        }
        else if(tekst.equals("Kierunek wiatru"))
        {
            jednostka = "deg";
        }
        tekst = tekst + " = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -1;
        }
        indeks = wartosc.indexOf(jednostka);
        wartosc = wartosc.substring(0,indeks);
        return Integer.parseInt(wartosc.trim());
    }

    public double podajWartoscDouble(String tekst, String ostatniOdczyt) // Temperatura, Cisnienie, Wilgotnosc
    {
        String jednostka="";
        if(tekst.equals("Cisnienie"))
        {
            jednostka = "hPA";
        }
        else if(tekst.equals("Temperatura"))
        {
            jednostka = "*C";
        }
        else if(tekst.equals("Wilgotnosc"))
        {
            jednostka = "%";
        }
        else if(tekst.equals("Predkosc wiatru"))
        {
            jednostka = "m/s";
        }
        else if(tekst.equals("Kierunek wiatru"))
        {
            jednostka = "deg";
        }
        tekst = tekst + " = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -1.0;
        }
        indeks = wartosc.indexOf(jednostka);
        wartosc = wartosc.substring(0,indeks);
        return Double.parseDouble(wartosc.trim());
    }

    public Wykresy() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Wykresy.
     */
    // TODO: Rename and change types and number of parameters
    public static Wykresy newInstance(String param1, String param2) {
        Wykresy fragment = new Wykresy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wykresy, container, false);

        frameLayoutWykresy = (FrameLayout) view.findViewById(R.id.frameLayoutWykresy);
        frameLayoutNaWykres = (FrameLayout) view.findViewById(R.id.frameLayoutNaWykres);

        spinnerRodzajDanych = (Spinner) view.findViewById(R.id.spinnerRodzajDanych);
        String[] rodzajeDanych = {"Temperatura", "Ciśnienie", "Wilgotność", "Prędkość wiatru", "Kierunek wiatru"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeDanych);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRodzajDanych.setAdapter(adapter);
        spinnerRodzajDanych.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
            //    rysujWykres();
                mListener.onFragmentInteraction("Wykresy", "Podaj dane");
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        spinnerRodzajWykresu = (Spinner) view.findViewById(R.id.spinnerRodzajWykresu);
        String[] rodzajeWykresow = {"Liniowy", "Słupkowy"};
        adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeWykresow);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRodzajWykresu.setAdapter(adapter);
        spinnerRodzajWykresu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //rysujWykres();
                // Lub
                mListener.onFragmentInteraction("Wykresy", "Podaj dane");
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });


        editTextIloscProbek = (EditText) view.findViewById(R.id.editTextIloscProbek);
        iloscProbekNaWykresie = Integer.parseInt(editTextIloscProbek.getText().toString());
        editTextIloscProbek.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                String napis = editTextIloscProbek.getText().toString();
                if(!napis.equals(""))
                {
                    iloscProbekNaWykresie = Integer.parseInt(napis);
                }
                mListener.onFragmentInteraction("Wykresy", "Podaj dane");
            }
        });

        mListener.onFragmentInteraction("Wykresy","Podaj dane");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}