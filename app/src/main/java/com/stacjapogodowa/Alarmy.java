package com.stacjapogodowa;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

public class Alarmy extends Fragment implements FragmentCallbacks
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private FrameLayout frameLayout;

    private Spinner spinnerTemperatura;
    private Spinner spinnerCisnienie;
    private Spinner spinnerWilgotnosc;
    private Spinner spinnerWiatr;

    private EditText editTextTemperaturaMax;
    private EditText editTextTemperaturaMin;
    private EditText editTextCisnienieMax;
    private EditText editTextCisnienieMin;
    private EditText editTextWilgotnoscMax;
    private EditText editTextWilgotnoscMin;
    private EditText editTextWiatrMax;
    private EditText editTextWiatrMin;

    private String alarmy = "T=0,25,5;P=0,1015,995;R=0,80,60;S=0,10,2";

    private TextWatcher textWatcher;
    private AdapterView.OnItemSelectedListener adapterListener;

    private boolean zmianaUzytkownika = true;

    @Override
    public void onMsgFromMainToFragment(String strValue)
    {
        alarmy = strValue;
        uzupelnijElementyGUI();
    }

    public void uzupelnijElementyGUI() // Na podstawie napisu alarmy
    {
        zmianaUzytkownika = false;

        String[] lista = alarmy.split(";");
        for(int i = 0; i < 3; i++)
        {
            lista[i] = lista[i].substring(2);
        }
        String[] listaTemperatura = lista[0].split(",");
        if(listaTemperatura[0].equals("1"))
        {
            spinnerTemperatura.setSelection(0);
        }
        else
        {
            spinnerTemperatura.setSelection(1);
        }
        editTextTemperaturaMax.setText(listaTemperatura[1]);
        editTextTemperaturaMin.setText(listaTemperatura[2]);

        String[] listaCisnienie = lista[1].split(",");
        if(listaCisnienie[0].equals("1"))
        {
            spinnerCisnienie.setSelection(0);
        }
        else
        {
            spinnerCisnienie.setSelection(1);
        }
        editTextCisnienieMax.setText(listaCisnienie[1]);
        editTextCisnienieMin.setText(listaCisnienie[2]);

        String[] listaWilgotnosc = lista[2].split(",");
        if(listaWilgotnosc[0].equals("1"))
        {
            spinnerWilgotnosc.setSelection(0);
        }
        else
        {
            spinnerWilgotnosc.setSelection(1);
        }
        editTextWilgotnoscMax.setText(listaWilgotnosc[1]);
        editTextWilgotnoscMin.setText(listaWilgotnosc[2]);

        String[] listaWiatr = lista[3].split(",");
        if(listaWiatr[0].equals("1"))
        {
            spinnerWiatr.setSelection(0);
        }
        else
        {
            spinnerWiatr.setSelection(1);
        }
        editTextWiatrMax.setText(listaWiatr[1]);
        editTextWiatrMin.setText(listaWiatr[2]);

        zmianaUzytkownika = true;
    }

    public Alarmy() {
        // Required empty public constructor
    }

    public static Alarmy newInstance(String param1, String param2) {
        Alarmy fragment = new Alarmy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_alarmy, container, false);

        frameLayout = (FrameLayout) view.findViewById(R.id.frameLayoutAlarmy);

        spinnerTemperatura = (Spinner) view.findViewById(R.id.spinnerAlarmTemperatury);
        String[] rodzajeDanych = {"Alarm temperatury włączony", "Alarm temperatury wyłączony"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeDanych);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTemperatura.setAdapter(adapter);
        editTextTemperaturaMax = (EditText) view.findViewById(R.id.editTextTMax);
        editTextTemperaturaMin = (EditText) view.findViewById(R.id.editTextTMin);

        spinnerCisnienie = (Spinner) view.findViewById(R.id.spinnerAlarmCisnienia);
        String[] rodzajeDanych2 = {"Alarm ciśnienia włączony", "Alarm ciśnienia wyłączony"};
        adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeDanych2);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCisnienie.setAdapter(adapter);
        editTextCisnienieMax = (EditText) view.findViewById(R.id.editTextCisnienieMax);
        editTextCisnienieMin = (EditText) view.findViewById(R.id.editTextCisnienieMin);

        spinnerWilgotnosc = (Spinner) view.findViewById(R.id.spinnerAlarmWilgotnosci);
        String[] rodzajeDanych3 = {"Alarm wilgotności włączony", "Alarm wilgotności wyłączony"};
        adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeDanych3);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWilgotnosc.setAdapter(adapter);
        editTextWilgotnoscMax = (EditText) view.findViewById(R.id.editTextWilgotnoscMax);
        editTextWilgotnoscMin = (EditText) view.findViewById(R.id.editTextWilgotnoscMin);


        spinnerWiatr = (Spinner) view.findViewById(R.id.spinnerAlarmWiatr);
        String[] rodzajeDanych4 = {"Alarm prędkości wiatru włączony", "Alarm prędkości wiatru wyłączony"};
        adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, rodzajeDanych4);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWiatr.setAdapter(adapter);
        editTextWiatrMax = (EditText) view.findViewById(R.id.editTextWiatrMax);
        editTextWiatrMin = (EditText) view.findViewById(R.id.editTextWiatrMin);

        adapterListener = new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(zmianaUzytkownika == true)
                {
                    sprawdzPoprawnoscDanychOAlarmach();
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        };
        spinnerTemperatura.setOnItemSelectedListener(adapterListener);
        spinnerCisnienie.setOnItemSelectedListener(adapterListener);
        spinnerWilgotnosc.setOnItemSelectedListener(adapterListener);
        spinnerWiatr.setOnItemSelectedListener(adapterListener);

        textWatcher = new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s)
            {
                if(zmianaUzytkownika == true)
                {
                    sprawdzPoprawnoscDanychOAlarmach();
                }
            }
        };

        editTextTemperaturaMax.addTextChangedListener(textWatcher);
        editTextTemperaturaMin.addTextChangedListener(textWatcher);
        editTextCisnienieMax.addTextChangedListener(textWatcher);
        editTextCisnienieMin.addTextChangedListener(textWatcher);
        editTextWilgotnoscMax.addTextChangedListener(textWatcher);
        editTextWilgotnoscMin.addTextChangedListener(textWatcher);
        editTextWiatrMax.addTextChangedListener(textWatcher);
        editTextWiatrMin.addTextChangedListener(textWatcher);

        return view;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mListener.onFragmentInteraction("Alarmy","Podaj dane");
    }

    @Override
    public void onDestroyView()
    {
        przygotujDaneOAlarmach();
        mListener.onFragmentInteraction("Alarmy",alarmy);
        super.onDestroyView();
    }

    @Override
    public void onDestroy()
    {
 //       przygotujDaneOAlarmach();
 //       mListener.onFragmentInteraction("Alarmy",alarmy);
        super.onDestroy();
    }

    void przygotujDaneOAlarmach()
    {
        alarmy = "T=";
        if(spinnerTemperatura.getSelectedItem().toString().equals("Alarm temperatury włączony"))
        {
            alarmy +="1,";
        }
        else
        {
            alarmy +="0,";
        }
        alarmy+=editTextTemperaturaMax.getText().toString() + ",";
        alarmy+=editTextTemperaturaMin.getText().toString() + ";";

        alarmy +="P=";
        if(spinnerCisnienie.getSelectedItem().toString().equals("Alarm ciśnienia włączony"))
        {
            alarmy +="1,";
        }
        else
        {
            alarmy +="0,";
        }
        alarmy+=editTextCisnienieMax.getText().toString() + ",";
        alarmy+=editTextCisnienieMin.getText().toString() + ";";

        alarmy +="R=";
        if(spinnerWilgotnosc.getSelectedItem().toString().equals("Alarm wilgotności włączony"))
        {
            alarmy +="1,";
        }
        else
        {
            alarmy +="0,";
        }
        alarmy+=editTextWilgotnoscMax.getText().toString() + ",";
        alarmy+=editTextWilgotnoscMin.getText().toString() + ";";

        alarmy +="S=";
        if(spinnerWiatr.getSelectedItem().toString().equals("Alarm prędkości wiatru włączony"))
        {
            alarmy +="1,";
        }
        else
        {
            alarmy +="0,";
        }
        alarmy+=editTextWiatrMax.getText().toString() + ",";
        alarmy+=editTextWiatrMin.getText().toString();
    }

    void sprawdzPoprawnoscDanychOAlarmach()
    {
        zmianaUzytkownika = false;
        if(spinnerTemperatura != null && spinnerTemperatura.getSelectedItem().toString().equals("Alarm temperatury włączony"))
        {
            int max,min;
            if(editTextTemperaturaMax!=null && editTextTemperaturaMin != null)
            {
                String maxS = editTextTemperaturaMax.getText().toString();
                String minS = editTextTemperaturaMin.getText().toString();
                if(maxS.equals("") || minS.equals(""))
                {
                    spinnerTemperatura.setSelection(1);
                }
                else
                {
                    max = (int) Double.parseDouble(maxS);
                    min = (int) Double.parseDouble(minS);
                    if(max <= min)
                    {
                        max = min+1;
                    }
                    editTextTemperaturaMax.setText(Integer.toString(max));
                    editTextTemperaturaMin.setText(Integer.toString(min));
                }
            }
        }

        if(spinnerCisnienie != null && spinnerCisnienie.getSelectedItem().toString().equals("Alarm ciśnienia włączony"))
        {
            int max,min;
            if(editTextCisnienieMax!=null && editTextCisnienieMin != null)
            {
                String maxS = editTextCisnienieMax.getText().toString();
                String minS = editTextCisnienieMin.getText().toString();
                if(maxS.equals("") || minS.equals(""))
                {
                    spinnerCisnienie.setSelection(1);
                }
                else
                {
                    max = (int) Double.parseDouble(maxS);
                    min = (int) Double.parseDouble(minS);
                    if(max <= min)
                    {
                        max = min+1;
                    }
                    editTextCisnienieMax.setText(Integer.toString(max));
                    editTextCisnienieMin.setText(Integer.toString(min));
                }
            }
        }

        if(spinnerWilgotnosc != null && spinnerWilgotnosc.getSelectedItem().toString().equals("Alarm wilgotności włączony"))
        {
            int max,min;
            if(editTextWilgotnoscMax!=null && editTextWilgotnoscMin != null)
            {
                String maxS = editTextWilgotnoscMax.getText().toString();
                String minS = editTextWilgotnoscMin.getText().toString();
                if(maxS.equals("") || minS.equals(""))
                {
                    spinnerWilgotnosc.setSelection(1);
                }
                else
                {
                    max = (int) Double.parseDouble(maxS);
                    min = (int) Double.parseDouble(minS);
                    if(max <= min)
                    {
                        max=min+1;
                    }
                    editTextWilgotnoscMax.setText(Integer.toString(max));
                    editTextWilgotnoscMin.setText(Integer.toString(min));
                }
            }
        }
        if(spinnerWiatr != null && spinnerWiatr.getSelectedItem().toString().equals("Alarm prędkości wiatru włączony"))
        {
            double max,min;
            if(editTextWiatrMax!=null && editTextWiatrMin!= null)
            {
                String maxS = editTextWiatrMax.getText().toString();
                String minS = editTextWiatrMin.getText().toString();
                if(maxS.equals("") || minS.equals(""))
                {
                    spinnerWiatr.setSelection(1);
                }
                else
                {
                    max=Double.parseDouble(editTextWiatrMax.getText().toString());
                    min=Double.parseDouble(editTextWiatrMin.getText().toString());
                    if(max <= min)
                    {
                        max = min+0.1;
                    }
                    editTextWiatrMax.setText(Double.toString(max));
                    editTextWiatrMin.setText(Double.toString(min));
                }
            }
        }
        zmianaUzytkownika=true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}