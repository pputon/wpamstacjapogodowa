package com.stacjapogodowa;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Ustawienia extends Fragment implements FragmentCallbacks
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private EditText editTextOkresProbkowania;
    private int okres=5;

    @Override
    public void onMsgFromMainToFragment(String strValue)
    {
        // Funkcja wywolywana z aktywnosci
    }

    public Ustawienia() {
        // Required empty public constructor
    }

    public static Ustawienia newInstance(String param1, String param2) {
        Ustawienia fragment = new Ustawienia();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_ustawienia, container, false);

        editTextOkresProbkowania = (EditText) view.findViewById(R.id.editTextOkresProbkowania);
        editTextOkresProbkowania.setText(Integer.toString(((MainActivity) getActivity()).podajOkresProbkowania()));
        editTextOkresProbkowania.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                String tekst = editTextOkresProbkowania.getText().toString();

                int wartoscCalkowita = 5;
                if(!tekst.equals(""))
                {
                    double wartosc = Double.parseDouble(tekst);
                    if(wartosc <= 0)
                    {
                        wartosc = 5.0;
                    }
                    wartoscCalkowita = (int) wartosc;
                }
                editTextOkresProbkowania.setText(Integer.toString(wartoscCalkowita));
                okres = wartoscCalkowita;
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView()
    {
        String tekst = editTextOkresProbkowania.getText().toString();

        int wartoscCalkowita = 5;
        if(!tekst.equals(""))
        {
            double wartosc = Double.parseDouble(tekst);
            if(wartosc <= 0)
            {
                wartosc = 5.0;
            }
            wartoscCalkowita = (int) wartosc;
        }
        editTextOkresProbkowania.setText(Integer.toString(wartoscCalkowita));
        okres = wartoscCalkowita;
        mListener.onFragmentInteraction("Ustawienia" , Integer.toString(okres));
        super.onDestroyView();
    }
}