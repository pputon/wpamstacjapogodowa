package com.stacjapogodowa;

import android.content.Context;
import android.graphics.Color;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.TimeUnit;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.Time;

public class menuGlowne1 extends Fragment implements FragmentCallbacks
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private ImageView imageViewNaslonecznienie;
    private ImageView imageViewTermometr;
    private ImageView imageViewBarometr;
    private ImageView imageViewWiatr;

    private TextView textViewJakoscPowietrza;
    private TextView textViewCzas;
    private TextView textViewTemperatura;
    private TextView textViewCisnienie;
    private TextView textViewWilgotnosc;
    private TextView textViewWiatr;

    private CountDownTimer timer;

    private String alarmy;
    private boolean alarmTemperatura;
    private int maxTemperatura;
    private int minTemperatura;
    private boolean alarmCisnienie;
    private int maxCisnienie;
    private int minCisnienie;
    private boolean alarmWilgotnosc;
    private int maxWilgotnosc;
    private int minWilgotnosc;
    private boolean alarmWiatr;
    private double maxWiatr;
    private double minWiatr;

    private int temperatura;
    private int cisnienie;
    private int wilgotnosc;
    private double predkoscWiatru;

    private final String czerwony = "#FFFF0000";
    private final String zielony = "#FF00FF00";
    private final String czarny = "#FF000000";


    @Override
    public void onMsgFromMainToFragment(String strValue)
    {
        // Funkcja wywolywana z aktywnosci

        // Jezeli poczatek wiadomosci ustawienia lub alarmy, to inne zachowanie
        if(strValue.substring(0,2).equals("T=")) // wiadomosc o alarmie
        {
            alarmy = strValue;
            uzupelnijDaneOAlarmach();
            return;
        }

        // Trzeba caly string strValue zamienic na wartosci, poustawiac zdjecia i wpisac teksty
        String naslonecznienie = podajNaslonecznienie(strValue);
        String opady = podajOpady(strValue);
        imageViewNaslonecznienie.setImageResource(podajIDNaslonecznienieIOpady(naslonecznienie,opady));

        String jakoscPowietrza = podajJakoscPowietrza(strValue);
        textViewJakoscPowietrza.setText(jakoscPowietrza);
        if(jakoscPowietrza.equals("DOBRA"))
        {
            textViewJakoscPowietrza.setTextColor(Color.parseColor(zielony));
        }
        else if(jakoscPowietrza.equals("ZLA"))
        {
            textViewJakoscPowietrza.setText("ZŁA");
            textViewJakoscPowietrza.setTextColor(Color.parseColor(czerwony));
        }
        else
        {
            textViewJakoscPowietrza.setText("ŚREDNIA");
            textViewJakoscPowietrza.setTextColor(Color.parseColor(czarny));
        }

        cisnienie = podajWartosc("Cisnienie",strValue);
        textViewCisnienie.setText(Integer.toString(cisnienie) + " hPa");
        if(alarmCisnienie && (cisnienie > maxCisnienie || cisnienie < minCisnienie))
        {
            textViewCisnienie.setTextColor(Color.parseColor(czerwony));
        }
        else
        {
            textViewCisnienie.setTextColor(Color.parseColor(czarny));
        }
        imageViewBarometr.setImageResource(podajIDCisnienie(cisnienie));

        temperatura = podajWartosc("Temperatura",strValue);
        textViewTemperatura.setText(Integer.toString(temperatura)+(char)0x00B0+"C");
        if(alarmTemperatura && ( temperatura > maxTemperatura || temperatura < minTemperatura ))
        {
            textViewTemperatura.setTextColor(Color.parseColor(czerwony));
        }
        else
        {
            textViewTemperatura.setTextColor(Color.parseColor(czarny));
        }
        imageViewTermometr.setImageResource(podajIDTemperatura(temperatura));

        predkoscWiatru = podajWartoscDouble("Predkosc wiatru",strValue);
        int kierunekWiatru = (int) podajWartoscDouble("Kierunek wiatru",strValue);
        textViewWiatr.setText(Double.toString(predkoscWiatru)+" m/s");
        if(alarmWiatr && ( predkoscWiatru > maxWiatr || predkoscWiatru < minWiatr ))
        {
            textViewWiatr.setTextColor(Color.parseColor(czerwony));
        }
        else
        {
            textViewWiatr.setTextColor(Color.parseColor(czarny));
        }
        imageViewWiatr.setImageResource(podajIDWiatr(predkoscWiatru));
        imageViewWiatr.setRotation(kierunekWiatru);

        wilgotnosc = podajWartosc("Wilgotnosc", strValue);
        textViewWilgotnosc.setText(Integer.toString(wilgotnosc)+" %");
        if(alarmWilgotnosc && ( wilgotnosc > maxWilgotnosc || wilgotnosc < minWilgotnosc ))
        {
            textViewWilgotnosc.setTextColor(Color.parseColor(czerwony));
        }
        else
        {
            textViewWilgotnosc.setTextColor(Color.parseColor(czarny));
        }
    }

    public void uzupelnijDaneOAlarmach()
    {
        String[] lista = alarmy.split(";");
        for(int i = 0; i < 3; i++)
        {
            lista[i] = lista[i].substring(2);
        }
        String[] listaTemperatura = lista[0].split(",");
        if(listaTemperatura[0].equals("1"))
        {
            alarmTemperatura = true;
            maxTemperatura = Integer.parseInt(listaTemperatura[1]);
            minTemperatura = Integer.parseInt(listaTemperatura[2]);
        }
        else
        {
            alarmTemperatura = false;
        }
        String[] listaCisnienie = lista[1].split(",");
        if(listaCisnienie[0].equals("1"))
        {
            alarmCisnienie = true;
            maxCisnienie = Integer.parseInt(listaCisnienie[1]);
            minCisnienie = Integer.parseInt(listaCisnienie[2]);
        }
        else
        {
            alarmCisnienie = false;
        }
        String[] listaWilgotnosc = lista[2].split(",");
        if(listaWilgotnosc[0].equals("1"))
        {
            alarmWilgotnosc = true;
            maxWilgotnosc = Integer.parseInt(listaWilgotnosc[1]);
            minWilgotnosc = Integer.parseInt(listaWilgotnosc[2]);
        }
        else
        {
            alarmWilgotnosc = false;
        }
        String[] listaWiatr = lista[3].split(",");
        if(listaWiatr[0].equals("1"))
        {
            alarmWiatr = true;
            maxWiatr = Double.parseDouble(listaWiatr[1]);
            minWiatr = Double.parseDouble(listaWiatr[2]);
        }
        else
        {
            alarmWiatr = false;
        }
    }

    public int podajIDNaslonecznienieIOpady(String naslonecznienie, String opady)
    {
        if(naslonecznienie.equals("Silne") && opady.equals("ulewne"))
        {
            return R.drawable.naslonecznienie_silne_opady_silne;
        }
        else if(naslonecznienie.equals("Silne") && opady.equals("slabe"))
        {
            return R.drawable.naslonecznienie_silne_opady_slabe;
        }
        else if(naslonecznienie.equals("Silne") && opady.equals("brak"))
        {
            return R.drawable.naslonecznienie_silne_opady_brak;
        }
        else if(naslonecznienie.equals("Srednie") && opady.equals("ulewne"))
        {
            return R.drawable.naslonecznienie_srednie_opady_silne;
        }
        else if(naslonecznienie.equals("Srednie") && opady.equals("slabe"))
        {
            return R.drawable.naslonecznienie_srednie_opady_slabe;
        }
        else if(naslonecznienie.equals("Srednie") && opady.equals("brak"))
        {
            return R.drawable.naslonecznienie_srednie_opady_brak;
        }
        else if(naslonecznienie.equals("Slabe") && opady.equals("ulewne"))
        {
            return R.drawable.naslonecznienie_slabe_opady_silne;
        }
        else if(naslonecznienie.equals("Slabe") && opady.equals("slabe"))
        {
            return R.drawable.naslonecznienie_slabe_opady_slabe;
        }
        else if(naslonecznienie.equals("Slabe") && opady.equals("brak"))
        {
            return R.drawable.naslonecznienie_slabe_opady_brak;
        }
        return R.drawable.naslonecznienie_srednie_opady_brak;
    }

    public int podajIDCisnienie(int cisnienie)
    {
        if(cisnienie < 995)
        {
            return R.drawable.barometr_niskie;
        }
        else if(cisnienie > 1020)
        {
            return R.drawable.barometr_wysokie;
        }
        else
        {
            return R.drawable.barometr_srednie;
        }
    }

    public int podajIDTemperatura(int temperatura)
    {
        if(temperatura < -20)
        {
            return R.drawable.termometr_minus20;
        }
        else if(temperatura < -15)
        {
            return R.drawable.termometr_minus15;
        }
        else if(temperatura < -10)
        {
            return R.drawable.termometr_minus10;
        }
        else if(temperatura < -5)
        {
            return R.drawable.termometr_minus5;
        }
        else if(temperatura < 0)
        {
            return R.drawable.termometr0;
        }
        else if(temperatura < 5)
        {
            return R.drawable.termometr5;
        }
        else if(temperatura < 10)
        {
            return R.drawable.termometr10;
        }
        else if(temperatura < 15)
        {
            return R.drawable.termometr15;
        }
        else if(temperatura < 20)
        {
            return R.drawable.termometr20;
        }
        else if(temperatura < 25)
        {
            return R.drawable.termometr25;
        }
        else if(temperatura < 30)
        {
            return R.drawable.termometr30;
        }
        else
        {
            return R.drawable.termometr35;
        }
    }

    public int podajIDWiatr(double predkoscWiatru)
    {
        if(predkoscWiatru < 6.0)
        {
            return R.drawable.wiatr_slaby;
        }
        else if(predkoscWiatru > 14.1)
        {
            return R.drawable.wiatr_silny;
        }
        else
        {
            return R.drawable.wiatr_sredni;
        }
    }

    public int podajWartosc(String tekst, String ostatniOdczyt) // Temperatura, Cisnienie, Wilgotnosc
    {
        String jednostka="";
        if(tekst.equals("Cisnienie"))
        {
            jednostka = "hPa";
        }
        else if(tekst.equals("Temperatura"))
        {
            jednostka = "*C";
        }
        else if(tekst.equals("Wilgotnosc"))
        {
            jednostka = "%";
        }
        else if(tekst.equals("Predkosc wiatru"))
        {
            jednostka = "m/s";
        }
        else if(tekst.equals("Kierunek wiatru"))
        {
            jednostka = "deg";
        }
        tekst = tekst + " = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -1;
        }
        indeks = wartosc.indexOf(jednostka);
        wartosc = wartosc.substring(0,indeks);
        return Integer.parseInt(wartosc.trim());
    }

    public double podajWartoscDouble(String tekst, String ostatniOdczyt) // Temperatura, Cisnienie, Wilgotnosc
    {
        String jednostka="";
        if(tekst.equals("Cisnienie"))
        {
            jednostka = "hPA";
        }
        else if(tekst.equals("Temperatura"))
        {
            jednostka = "*C";
        }
        else if(tekst.equals("Wilgotnosc"))
        {
            jednostka = "%";
        }
        else if(tekst.equals("Predkosc wiatru"))
        {
            jednostka = "m/s";
        }
        else if(tekst.equals("Kierunek wiatru"))
        {
            jednostka = "deg";
        }
        tekst = tekst + " = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -1.0;
        }
        indeks = wartosc.indexOf(jednostka);
        wartosc = wartosc.substring(0,indeks);
        return Double.parseDouble(wartosc.trim());
    }

    public String podajJakoscPowietrza(String ostatniOdczyt)
    {
        String tekst = "Jakosc powietrza: ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks+tekst.length());
        String[] wartosc = tekst.split("\r\n");
        return wartosc[0];
    }

    public String podajNaslonecznienie(String ostatniOdczyt)
    {
        String tekst = "naslonecznienie, ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks-tekst.length());
        String[] wartosc = tekst.split("\r\n");
        wartosc = wartosc[1].split(",");
        tekst = wartosc[0].replaceFirst("naslonecznienie", " ");
        return tekst.trim();
    }

    public String podajOpady(String ostatniOdczyt)
    {
        String tekst = "Jakosc powietrza: ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks+tekst.length());
        String[] wartosc = tekst.split("\r\n");
        wartosc = wartosc[1].split(",");

        tekst = wartosc[1].replaceFirst("opady", " ");
        return tekst.trim();
    }


    public menuGlowne1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment menuGlowne1.
     */
    // TODO: Rename and change types and number of parameters
    public static menuGlowne1 newInstance(String param1, String param2) {
        menuGlowne1 fragment = new menuGlowne1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.menu_glowne1_fragment, container, false);

        imageViewNaslonecznienie = (ImageView) view.findViewById(R.id.imageViewNaslonecznienie);
        imageViewNaslonecznienie.setImageResource(R.drawable.naslonecznienie_srednie_opady_brak);
        imageViewBarometr = (ImageView) view.findViewById(R.id.imageViewBarometr);
        imageViewBarometr.setImageResource(R.drawable.barometr_srednie);
        imageViewTermometr = (ImageView) view.findViewById(R.id.imageViewTermometr);
        imageViewTermometr.setImageResource(R.drawable.termometr25);
        imageViewWiatr = (ImageView) view.findViewById(R.id.imageViewWiatr);
        imageViewWiatr.setImageResource(R.drawable.wiatr_sredni);

        // TextView ponizej
        textViewCisnienie = (TextView) view.findViewById(R.id.textViewCisnienie);
        textViewCisnienie.setText("1005 hPa");

        textViewCzas = (TextView) view.findViewById(R.id.textViewCzas);

        long milliseconds = System.currentTimeMillis();
        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24); // Do sprawdzenia!!!
        hours = (hours + 2) % 24;
        String czas = String.format("%02d:%02d:%02d", hours, minutes,seconds);
        textViewCzas.setText(czas);

        timer = new CountDownTimer(1000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(alarmCisnienie && ( cisnienie > maxCisnienie || cisnienie < minCisnienie ))
                {
                    if(textViewCisnienie.getVisibility()==View.VISIBLE)
                    {
                        textViewCisnienie.setVisibility(View.GONE);
                    }
                    else
                    {
                        textViewCisnienie.setVisibility(View.VISIBLE);
                    }
                }
                if(alarmTemperatura && ( temperatura > maxTemperatura || temperatura < minTemperatura ))
                {
                    if(textViewTemperatura.getVisibility()==View.VISIBLE)
                    {
                        textViewTemperatura.setVisibility(View.GONE);
                    }
                    else
                    {
                        textViewTemperatura.setVisibility(View.VISIBLE);
                    }
                }
                if(alarmWilgotnosc && ( wilgotnosc > maxWilgotnosc || wilgotnosc < minWilgotnosc ))
                {
                    if(textViewWilgotnosc.getVisibility()==View.VISIBLE)
                    {
                        textViewWilgotnosc.setVisibility(View.GONE);
                    }
                    else
                    {
                        textViewWilgotnosc.setVisibility(View.VISIBLE);
                    }
                }
                if(alarmWiatr && ( predkoscWiatru > maxWiatr || predkoscWiatru < minWiatr ))
                {
                    if(textViewWiatr.getVisibility()==View.VISIBLE)
                    {
                        textViewWiatr.setVisibility(View.GONE);
                    }
                    else
                    {
                        textViewWiatr.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFinish()
            {
                long milliseconds = System.currentTimeMillis();
                int seconds = (int) (milliseconds / 1000) % 60 ;
                int minutes = (int) ((milliseconds / (1000*60)) % 60);
                int hours   = (int) (((milliseconds / (1000*60*60)) % 24)+2)%24; // Do sprawdzenia!!!

                String czas = String.format("%02d:%02d:%02d", hours, minutes,seconds);
                textViewCzas.setText(czas);
                timer.start();
            }
        }.start();

        textViewTemperatura = (TextView) view.findViewById(R.id.textViewTemperatura);
        textViewTemperatura.setText("27 "+(char)0x00B0+"C");
        textViewJakoscPowietrza = (TextView) view.findViewById(R.id.textViewJakoscPowietrza);
        textViewJakoscPowietrza.setText("ŚREDNIA");
        textViewWiatr = (TextView) view.findViewById(R.id.textViewWiatr);
        textViewWiatr.setText("0.0 m/s");
        textViewWilgotnosc = (TextView) view.findViewById(R.id.textViewWilgotnosc);
        textViewWilgotnosc.setText("65 %");
        return view;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mListener.onFragmentInteraction("menuGlowne1", "Podaj alarmy");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
  //  public interface OnFragmentInteractionListener {
  //      // TODO: Update argument type and name
  //      void onFragmentInteraction(String sender, String strValue);
//     }
}