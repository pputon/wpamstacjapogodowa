package com.stacjapogodowa;


import android.Manifest;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kwabenaberko.openweathermaplib.constants.Lang;
import com.kwabenaberko.openweathermaplib.constants.Units;
import com.kwabenaberko.openweathermaplib.implementation.OpenWeatherMapHelper;
import com.kwabenaberko.openweathermaplib.implementation.callbacks.CurrentWeatherCallback;
import com.kwabenaberko.openweathermaplib.models.currentweather.CurrentWeather;

import java.lang.ref.WeakReference;
import java.security.Provider;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener
{
    private UsbService usbService;

    private MyHandler mHandler;
    private OpenWeatherMapHelper helper;

    private LocationManager locationManager;
    private FragmentManager fragmentManager;
    private CountDownTimer timer;

    private String ostatniOdczyt="Aktualny czas: fd\r\nTemperatura = 26 *C\r\nCisnienie = 1003 hPa\r\nWilgotnosc = 60 %\r\nJakosc powietrza: DOBRA\r\nSilne naslonecznienie, ulewne opady\r\n";
    public CurrentWeather pogoda;

    private DatabaseReference reference;
    private ValueEventListener eventListener;

    private String alarmy = "T=0,25,5;P=0,1015,995;R=0,80,60;S=0,10,2";

//    private String ustawienia;
    private int okresProbkowania=5;
    private int licznik=5;
    private String odczyt = "";

    private boolean USBaktywne = false;

    @Override
    public void onFragmentInteraction(String sender, String strValue)
    {
        // Tutaj obsluga wiadomosci od fragmentow - na podstawie sender i strValue
        if(sender.equals("Wykresy"))
        {
            if(strValue.equals("Podaj dane"))
            {
                ValueEventListener eventListener = new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        List<String> list = new ArrayList<>();

                        for(DataSnapshot ds : dataSnapshot.getChildren())
                        {
                            String ID = ds.getKey();
                            list.add(ds.getValue(String.class));
                        }
                        Fragment fragment = fragmentManager.findFragmentById(R.id.frameLayout);
                        if(fragment instanceof Wykresy)
                        {
                            Wykresy fragmentWykresy = (Wykresy) fragment;
                            fragmentWykresy.onMsgFromMainToFragment(TextUtils.join(";",list));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError)
                    {

                    }
                };

                reference.addListenerForSingleValueEvent(eventListener); // Powinno dzialac, tam przekazanie danych
            }
        }
        else if(sender.equals("Alarmy"))
        {
            if(strValue.equals("Podaj dane")) // Trzeba wyslac dane o alarmach do alarmow
            {
                Fragment fragment = (Fragment) fragmentManager.findFragmentById(R.id.frameLayout);
                if(fragment instanceof Alarmy)
                {
                    Alarmy alarmy1 = (Alarmy) fragment;
                    alarmy1.onMsgFromMainToFragment(alarmy);
                }
            }
            else // Tutaj dostajemy dane o alarmach
            {
                alarmy = strValue;
            }
        }
        else if(sender.equals("menuGlowne1"))
        {
            if(strValue.equals("Podaj alarmy"))
            {
                Fragment fragment = (Fragment) fragmentManager.findFragmentById(R.id.frameLayout);
                if(fragment instanceof menuGlowne1)
                {
                    menuGlowne1 fragment1 = (menuGlowne1) fragment;
                    fragment1.onMsgFromMainToFragment(alarmy);
                }
            }
        }
        else if(sender.equals("Ustawienia"))
        {
            // strValue - czas probkowania
            okresProbkowania = Integer.parseInt(strValue);
        }
    }

    public String podajOdczytNaPodstawieOWM()
    {
        String napis = "Temperatura = ";
        napis += (int) pogoda.getMain().getTemp();
        napis += " *C\r\nCisnienie = ";
        napis += (int) pogoda.getMain().getPressure();
        napis += " hPa\r\nWilgotnosc = ";
        napis += (int) pogoda.getMain().getHumidity();
        napis += " %\r\nJakosc powietrza: SREDNIA\r\n";
        napis += "Silne naslonecznienie, brak opady\r\n"; // pogoda.getWeather().get(0).getDescription() - zachmurzenie
        return napis;
    }

    public int podajOkresProbkowania()
    {
        return okresProbkowania;
    }

    public CurrentWeather podajPogode() { return pogoda; }

    public String podajOdczyt() { return ostatniOdczyt; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();
        menuGlowne1 fragment = (menuGlowne1) fragmentManager.findFragmentById(R.id.frameLayout);
        if(fragment==null)
        {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment = new menuGlowne1();
            fragmentTransaction.replace(R.id.frameLayout, fragment).addToBackStack(null);
            fragmentTransaction.commit();
        }

        mHandler = new MyHandler(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) { }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) { }
            @Override
            public void onProviderEnabled(String provider) { }
            @Override
            public void onProviderDisabled(String provider) { }
        };

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // Nie ma dostepu
            Toast.makeText(this.getApplicationContext(), "Brak dostępu do lokalizacji", Toast.LENGTH_SHORT).show();
        }
        else
        {
            String provider = LocationManager.NETWORK_PROVIDER;
            locationManager.requestLocationUpdates(provider, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            Location location2 = locationManager.getLastKnownLocation(provider);
          /*  if(location2!=null)
            { }
            else
            { }
     */ }


        //Instantiate Class With Your ApiKey As The Parameter
        helper = new OpenWeatherMapHelper("8f317d701f183cf72cac659011cd3895");
        //Set Units
        helper.setUnits(Units.METRIC);

        //Set lang
        helper.setLang(Lang.POLISH);

        helper.getCurrentWeatherByCityName("Warsaw", new CurrentWeatherCallback() {
            @Override
            public void onSuccess(CurrentWeather currentWeather) {
                Log.v(TAG,
                        "Coordinates: " + currentWeather.getCoord().getLat() + ", "+currentWeather.getCoord().getLon() +"\n"
                                +"Weather Description: " + currentWeather.getWeather().get(0).getDescription() + "\n"
                                +"Temperature: " + currentWeather.getMain().getTempMax()+"\n"
                                +"Wind Speed: " + currentWeather.getWind().getSpeed() + "\n"
                                +"City, Country: " + currentWeather.getName() + ", " + currentWeather.getSys().getCountry()
                );
                pogoda = currentWeather;
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.v(TAG, throwable.getMessage());
            }
        });

        timer = new CountDownTimer(Long.MAX_VALUE, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished) // Co sekunde
            {
                if (licznik >= okresProbkowania - 1)
                {
                    byte tablica[] = {'A'};
                    String data = "t";
                    data = tablica.toString();
                    if (usbService != null) { // if UsbService was correctly binded, Send data
                        odczyt = "";
                        usbService.write(tablica);
                    }

                    try
                    {
                        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if(location!=null)
                        {
                            helper.getCurrentWeatherByGeoCoordinates(location.getLatitude(),location.getLongitude(),new CurrentWeatherCallback() {
                                @Override
                                public void onSuccess(CurrentWeather currentWeather) {
                                    Log.v(TAG, "Coordinates: " + currentWeather.getCoord().getLat() + ", " + currentWeather.getCoord().getLon() + "\n"
                                            + "Weather Description: " + currentWeather.getWeather().get(0).getDescription() + "\n"
                                            + "Temperature: " + currentWeather.getMain().getTempMax() + "\n"
                                            + "Wind Speed: " + currentWeather.getWind().getSpeed() + "\n"
                                            + "City, Country: " + currentWeather.getName() + ", " + currentWeather.getSys().getCountry()
                                    );
                                    pogoda = currentWeather;
                                }
                                @Override
                                public void onFailure(Throwable throwable) {
                                    Log.v(TAG, throwable.getMessage());
                                }
                            });
                        }
                    }
                    catch (SecurityException e)
                    {
                        e.printStackTrace();
                    }

                    // Mam zmienne: ostatniOdczyt i pogoda - trzeba je wyslac do fragmentu1
                    Fragment fragment = fragmentManager.findFragmentById(R.id.frameLayout);
                    if(USBaktywne == false && pogoda != null)
                    {
                        ostatniOdczyt = podajOdczytNaPodstawieOWM();
                    }
                    if(fragment instanceof menuGlowne1 && pogoda!=null)
                    {
                        menuGlowne1 fragment2 = (menuGlowne1) fragment;
                        fragment2.onMsgFromMainToFragment(ostatniOdczyt + "Kierunek wiatru = " + pogoda.getWind().getDeg() + " deg\r\nPredkosc wiatru = " + pogoda.getWind().getSpeed() + " m/s\r\n");
                    }
                    else if(fragment instanceof menuGlowne1)
                    {
                        menuGlowne1 fragment2 = (menuGlowne1) fragment;
                        fragment2.onMsgFromMainToFragment(ostatniOdczyt + "Kierunek wiatru = " + 0.0 + " deg\r\nPredkosc wiatru = " + 0.0 + " m/s\r\n");
                    }
                    zapiszDoBazyDanych();
                    licznik = 0;
                }
                else
                {
                    if(licznik==0 && !odczyt.equals(""))
                    {
                        ostatniOdczyt = odczyt;
                        USBaktywne = true;
                    }
                    else if(licznik == 0)
                    {
                        USBaktywne = false;
                    }
                    licznik=licznik+1;
                }
            }

            @Override
            public void onFinish()
            {
                timer.start();
            }
        }.start();
        // Jeszcze baza danych do skonfigurowania
        reference = FirebaseDatabase.getInstance().getReference();

        ValueEventListener eventListener = new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                List<String> list = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    String ID = ds.getKey();
                    list.add(ds.getValue(String.class));
                }
                Fragment fragment = fragmentManager.findFragmentById(R.id.frameLayout);
                if(fragment instanceof Wykresy)
                {
                    Wykresy fragmentWykresy = (Wykresy) fragment;
                    fragmentWykresy.onMsgFromMainToFragment(TextUtils.join(";",list));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        };

    //    reference.addListenerForSingleValueEvent(eventListener);
    }


    public void zapiszDoBazyDanych()
    {
        // Zapis do Firebase'a - w wykresach bedzie odczyt
        // reference sie nazywa instancja DatabaseReference
        long milliseconds = System.currentTimeMillis();
        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24); // Do sprawdzenia!!!
        
        String napis = String.format("%02d:%02d:%02d\r\n", hours, minutes,seconds);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        napis += formatter.format(new Date(milliseconds));
        napis += "\r\n";

        if(pogoda != null) // Jeszcze czas dolozyc
        {
            napis += ostatniOdczyt + "Kierunek wiatru = " + pogoda.getWind().getDeg() + " deg\r\nPredkosc wiatru = " + pogoda.getWind().getSpeed() + " m/s\r\n";
        }
        else
        {
            napis += ostatniOdczyt + "Kierunek wiatru = " + 0.0 + " deg\r\nPredkosc wiatru = " + 0.0 + " m/s\r\n";
        }
        reference.push().setValue(napis);
    }

    /*
     * Notifications from UsbService will be received here.
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Toast.makeText(context, "USB gotowe", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "Nie otrzymano zezwolenia na komunikację USB", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "Brak urządzenia USB", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Toast.makeText(context, "USB rozłączone", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Toast.makeText(context, "USB urządzenie niewspierane", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public static final String TAG = MainActivity.class.getSimpleName();


    public int podajTemperatureZMikrokontrolera() // Dobrze dziala - analogicznie kolejne
    {
        String tekst = "Temperatura = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -273;
        }
        indeks = wartosc.indexOf("*C");
        wartosc = wartosc.substring(0,indeks);
        return Integer.parseInt(wartosc.trim());
    }

    public int podajWartoscZMikrokontrolera(String tekst) // Temperatura, Cisnienie, Wilgotnosc
    {
        String jednostka="";
        if(tekst.equals("Cisnienie"))
        {
            jednostka = "hPa";
        }
        else if(tekst.equals("Temperatura"))
        {
            jednostka = "*C";
        }
        else if(tekst.equals("Wilgotnosc"))
        {
            jednostka = "%";
        }
        tekst = tekst + " = ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        String wartosc;
        if(indeks>=0)
        {
            wartosc = ostatniOdczyt.substring(indeks+tekst.length());
        }
        else
        {
            return -1;
        }
        indeks = wartosc.indexOf(jednostka);
        wartosc = wartosc.substring(0,indeks);
        return Integer.parseInt(wartosc.trim());
    }

    public String podajJakoscPowietrza()
    {
        String tekst = "Jakosc powietrza: ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks+tekst.length());
        String[] wartosc = tekst.split("\r\n");
        return wartosc[0];
    }

    public String podajNaslonecznienie()
    {
        String tekst = "Jakosc powietrza: ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks+tekst.length());
        String[] wartosc = tekst.split("\r\n");
        wartosc = wartosc[0].split(",");
        tekst = "naslonecznienie";
        indeks = wartosc[0].indexOf(tekst);
        tekst = wartosc[0].substring(indeks+tekst.length());
        return tekst.trim();
    }

    public String podajOpady()
    {
        String tekst = "Jakosc powietrza: ";
        int indeks = ostatniOdczyt.indexOf(tekst);
        tekst = ostatniOdczyt.substring(indeks+tekst.length());
        String[] wartosc = tekst.split("\r\n");
        wartosc = wartosc[1].split(",");
        tekst = "opady";
        indeks = wartosc[0].indexOf(tekst);
        tekst = wartosc[0].substring(indeks+tekst.length());
        return tekst.trim();
    }


    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(mHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    public void przelaczRamke1(int idMenuSelection)
    {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
        {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        Fragment fragment = fragmentManager.findFragmentById(R.id.frameLayout);
        if(idMenuSelection == R.id.action_Szczegoly) // Wybieram szczegoly, to zmieniam na fragment2
        {
            if(!(fragment instanceof menuGlowne2))
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                menuGlowne2 fragment2 = new menuGlowne2();
                fragmentTransaction.replace(R.id.frameLayout, fragment2).addToBackStack(null);
                fragmentTransaction.commit();
            }
        }
        else if(idMenuSelection == R.id.action_glowneMenu)
        {
            if(!(fragment instanceof menuGlowne1))
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                menuGlowne1 fragment2 = new menuGlowne1();
                fragmentTransaction.replace(R.id.frameLayout, fragment2).addToBackStack(null);
                fragmentTransaction.commit();

                CountDownTimer timer2 = new CountDownTimer(100,50) {
                    @Override
                    public void onTick(long millisUntilFinished) { }
                    @Override
                    public void onFinish() {
                        if(pogoda!=null)
                        {
                            fragment2.onMsgFromMainToFragment(ostatniOdczyt + "Kierunek wiatru = " + pogoda.getWind().getDeg() + " deg\r\nPredkosc wiatru = " + pogoda.getWind().getSpeed() + " m/s\r\n");
                        }
                        else
                        {
                            fragment2.onMsgFromMainToFragment(ostatniOdczyt + "Kierunek wiatru = " + 0.0 + " deg\r\nPredkosc wiatru = " + 0.0 + " m/s\r\n");
                        }
                    }
                }.start();
            }
        }
        else if(idMenuSelection == R.id.actions_plot)
        {
            if(!(fragment instanceof Wykresy )) // Wtedy przelaczenie, w przeciwnym przypadku nic
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Wykresy fragmentWykresy = new Wykresy();
                fragmentTransaction.replace(R.id.frameLayout, fragmentWykresy).addToBackStack(null);
                fragmentTransaction.commit();
            }
        }
        else if(idMenuSelection == R.id.action_alarmy)
        {
            if(!(fragment instanceof Alarmy )) // Wtedy przelaczenie, w przeciwnym przypadku nic
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Alarmy fragmentAlarmy = new Alarmy();
                fragmentTransaction.replace(R.id.frameLayout, fragmentAlarmy).addToBackStack(null);
                fragmentTransaction.commit();
            }
        }
        else if(idMenuSelection == R.id.action_settings)
        {
            if(!(fragment instanceof Ustawienia )) // Wtedy przelaczenie, w przeciwnym przypadku nic
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Ustawienia fragmentUstawienia = new Ustawienia();
                fragmentTransaction.replace(R.id.frameLayout, fragmentUstawienia).addToBackStack(null);
                fragmentTransaction.commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        przelaczRamke1(item.getItemId());
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        setFilters();  // Start listening notifications from UsbService
        startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    /*
     * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
     */
    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = (String) msg.obj;
                    mActivity.get().odczyt += data; // Tutaj odbior danych
                    break;
                case UsbService.CTS_CHANGE:
                    Toast.makeText(mActivity.get(), "CTS_CHANGE",Toast.LENGTH_LONG).show();
                    break;
                case UsbService.DSR_CHANGE:
                    Toast.makeText(mActivity.get(), "DSR_CHANGE",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}