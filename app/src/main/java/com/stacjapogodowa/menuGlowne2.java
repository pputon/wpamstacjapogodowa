package com.stacjapogodowa;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.kwabenaberko.openweathermaplib.models.common.Main;
import com.kwabenaberko.openweathermaplib.models.currentweather.CurrentWeather;

import java.util.List;

public class menuGlowne2 extends Fragment implements FragmentCallbacks
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView listaSzczegolow;
    private ArrayAdapter<String> adapter;

    private CountDownTimer timer;
    private String[] wartosci= {"Temperatura = 27 " + (char)0x00B0+"C","Ciśnienie = 1013 hPA",
            "Wilgotność względna = 60 %",
            "Prędkość wiatru = 1.4 m/s",
            "Kierunek wiatru = 70 " + (char)0x00B0,
            "Zachmurzenia : bezchmurnie",
            "Wschód słońca : 05:34",
            "Zachód słońca : 21:13"};

    @Override
    public void onMsgFromMainToFragment(String strValue)
    {

    }

    public menuGlowne2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment menuGlowne2.
     */
    // TODO: Rename and change types and number of parameters
    public static menuGlowne2 newInstance(String param1, String param2) {
        menuGlowne2 fragment = new menuGlowne2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.menu_glowne2_fragment, container, false);

        listaSzczegolow = (ListView) view.findViewById(R.id.listaSzczegolow);


        adapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,wartosci);
        listaSzczegolow.setAdapter(adapter);

        timer = new CountDownTimer(Long.MAX_VALUE, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished) // Co sekunde
            {
                // Tutaj odswiezenie listy
                CurrentWeather pogoda =((MainActivity) getActivity()).podajPogode();
                String odczyt = ((MainActivity) getActivity()).podajOdczyt();

                wartosci[0] = "Temperatura = " + ((MainActivity) getActivity()).podajWartoscZMikrokontrolera("Temperatura") + " " + (char)0x00B0 + "C";
                wartosci[1] = "Ciśnienie = " + ((MainActivity) getActivity()).podajWartoscZMikrokontrolera("Cisnienie") + " hPa";
                wartosci[2] = "Wilgotność względna = " + ((MainActivity) getActivity()).podajWartoscZMikrokontrolera("Wilgotnosc") + " %";
                if(pogoda != null)
                {
                    wartosci[3] = "Prędkość wiatru = " + pogoda.getWind().getSpeed() + " m/s";
                    wartosci[4] = "Kierunek wiatru = " + pogoda.getWind().getDeg() + " " + (char)0x00B0;
                    wartosci[5] = "Zachmurzenie : " + pogoda.getWeather().get(0).getDescription();
                }
                else
                {
                    wartosci[3] = "Prędkość wiatru =  0.0 m/s";
                    wartosci[4] = "Kierunek wiatru = 0"  + (char)0x00B0;
                    wartosci[5] = "Zachmurzenie : slabe";
                }

                long aktualnyCzas = System.currentTimeMillis(); // Przeliczenie na sekundy
                int minutes = (int) ((aktualnyCzas / (1000*60)) % 60);
                int hours   = (int) ((aktualnyCzas / (1000*60*60)) % 24); // Do sprawdzenia!!!

                long wschod, zachod;
                if(pogoda!= null)
                {
                    wschod = pogoda.getSys().getSunrise();
                    zachod = pogoda.getSys().getSunset();
                }
                else
                {
                    wschod = 1560343791;
                    zachod = 1560343791;
                }

                minutes = (int) ((wschod/ (60)) % 60);
                hours   = (int) (((wschod/ (60*60)) % 24)+2)%24; // Do sprawdzenia!!!
                String czas = String.format("%02d:%02d", hours, minutes);
                wartosci[6] = "Wschód słońca : " + czas;

                minutes = (int) ((zachod / (60)) % 60);
                hours   = (int) (((zachod / (60*60)) % 24)+2)%24; // Do sprawdzenia!!!
                czas = String.format("%02d:%02d", hours, minutes);
                wartosci[7] = "Zachód słońca : " + czas;

                adapter.notifyDataSetChanged();
            }
            @Override
            public void onFinish() { timer.start(); }
        }.start();


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(String sender, String strValue);
//    }
}