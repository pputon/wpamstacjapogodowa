package com.stacjapogodowa;

public interface FragmentCallbacks {
    public void onMsgFromMainToFragment(String strValue);
}
