package com.stacjapogodowa;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(String sender, String strValue);
}
